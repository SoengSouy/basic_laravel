<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',function(){
    return redirect()->route('form/personal/new');
})->name('/');

// route test 
Route::get('form/personal/new',[App\Http\Controllers\TestController::class,'viewTest'])->name('form/personal/new');
Route::post('form/page_test/save',[App\Http\Controllers\TestController::class,'viewTestSave'])->name('form/page_test/save');




Route::post('form/update',[App\Http\Controllers\TestController::class,'update'])->name('form/update');
Route::get('form/view/{id}',[App\Http\Controllers\TestController::class,'view']);
Route::get('form/delete{id}',[App\Http\Controllers\TestController::class,'delete']);
