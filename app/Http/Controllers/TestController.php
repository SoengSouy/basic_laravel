<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Personal;
use Auth;

class TestController extends Controller
{
    //
    public function viewTest()
    {
        $data = Personal::all();
        return view('form.form',compact('data'));
    }
    // save
    public function viewTestSave(Request $request)
    {
        $request->validate([
            'username'=>'required|max:100',
            'email'   =>'required|email|unique:personals',
            'phone'   =>'required|min:11|numeric',
            'fileupload'   =>'required',
        ]);
    
        try{

            $fileupload = $request->fileupload;
            $path = 'uploads/';
            $file_name  = $fileupload->getClientOriginalName();
            $fileupload->move($path,$file_name);

            $Personal = new Personal();
            $Personal->username = $request->username;
            $Personal->email    = $request->email;
            $Personal->phone    = $request->phone;
            $Personal->fileupload= $file_name;
            $Personal->save();
            return redirect()->back()->with('insert','Data has been insert successfully!.');

        }catch(\Exception $e){

            return redirect()->back()->with('error','Data has been insert fail!.');
        }
    }
    // view detail
    public function view()
    {
        $data = Personal::all();
        return view('form.viewform',compact('data'));
    }
    // update
    public function update(Request $request)
    {
        $update =[

            'id'      =>$request->id,
            'username'=>$request->name,
            'email'   =>$request->email,
            'phone'   =>$request->phone,
        ];
        Personal::where('id',$request->id)->update($update);
        return redirect()->route('form/personal/new')->with('insert','Data has been updated successfully!.');
    }

    // delete
    public function delete($id)
    {
        $delete = Personal::find($id);
        $delete->delete();
        return redirect()->back()->with('insert','Data has been deleted successfully!.');
    }
    
}
